CREATE TABLE `carreras`.`resultado_carrera` ( `id` INT NOT NULL AUTO_INCREMENT , `resultado` VARCHAR(500) NOT NULL , PRIMARY KEY (`id`)) ENGINE = MyISAM;

CREATE TABLE `carreras`.`conductor` ( `id` INT NOT NULL AUTO_INCREMENT , `nombre` VARCHAR(255) NOT NULL , `veces_ganadas` INT NOT NULL DEFAULT '0' , PRIMARY KEY (`id`, `veces_ganadas`), UNIQUE (`nombre`)) ENGINE = MyISAM;
