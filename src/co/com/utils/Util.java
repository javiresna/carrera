package co.com.utils;

import java.util.ArrayList;

public class Util {

  public int dado6Caras() {
    int min = 1;
    int max = 6;
    return (int) (Math.random() * (max - min + 1) + min);
  }

  public int calcularCarriles() {
    int min = 4;
    int max = 10;
    return (int) (Math.random() * (max - min + 1) + min);
  }

  public int calcularDistanciaKm() {
    int min = 10;
    int max = 70;
    return (int) (Math.random() * (max - min + 1) + min);
  }

  public ArrayList<String> conductores() {
    ArrayList<String> condictores = new ArrayList<>();
    condictores.add("Montoya");
    condictores.add("Hamilton");
    condictores.add("Alonso");
    condictores.add("Bottas");
    condictores.add("Vettel");
    condictores.add("Verstappen");
    condictores.add("Perez");
    condictores.add("Norris");
    condictores.add("Ricciardo");
    condictores.add("Stroll");
    return condictores;
  }





}
