package co.com.clases;

import co.com.db.ConexionMysql;
import co.com.dto.Carro;
import co.com.dto.Podio;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Resultado {

  private ConexionMysql mysql;

  public Resultado() {
    this.mysql = new ConexionMysql();
  }

  public void registrarResultadosCarrera(Podio podio) {
    String sql = "INSERT INTO resultado_carrera(resultado) values ('";
    sql =
        sql + "Primer Lugar " + podio.getPrimerLugar().getConductor().getNombre() + " en el carro: "
            + podio.getPrimerLugar().getNumeroCarro() + ", ";
    sql = sql + "Segundo Lugar " + podio.getSegundoLugar().getConductor().getNombre()
        + " en el carro: " + podio.getTercerLugar().getNumeroCarro() + ", ";
    sql =
        sql + "Tercer Lugar " + podio.getTercerLugar().getConductor().getNombre() + " en el carro: "
            + podio.getTercerLugar().getNumeroCarro();
    sql = sql + "')";
    mysql.conectarMysql();
    boolean resultado = mysql.ejecutarConsulta(sql);
    if (resultado) {
      System.out.println("Se guardaron los resultados de la carrera correctamente en mysql");
    } else {
      System.out.println("Error guardando la informacion");
    }
    mysql.cerrarMysql();
  }

  public void registrarPrimerLugar(Carro ganador) {
    String sql =
        "SELECT * FROM conductor WHERE conductor.nombre = '" + ganador.getConductor().getNombre()
            + "'";
    mysql.conectarMysql();
    ResultSet resultSet = mysql.consultar(sql);
    try {
      if (!resultSet.next()) {
        String nSql = "INSERT INTO conductor(nombre, veces_ganadas) values (";
        nSql = nSql + "'" + ganador.getConductor().getNombre() + "', " + 1;
        nSql = nSql + ")";
        boolean resultado = mysql.ejecutarConsulta(nSql);
        if (resultado) {
          System.out.println("Se guardaron los datos del ganador correctamente en mysql");
        } else {
          System.out.println("Error guardando la informacion");
        }
      } else {
        do {
          int vecesGanadas = resultSet.getInt("veces_ganadas");
          vecesGanadas++;
          String nSql =
              "UPDATE conductor SET veces_ganadas =" + vecesGanadas + " WHERE nombre='" + ganador
                  .getConductor().getNombre() + "'";
          boolean resultado = mysql.ejecutarConsulta(nSql);
          if (resultado) {
            System.out.println("Se actualizaron los datos del ganador correctamente en mysql");
          } else {
            System.out.println("Error actualizando la informacion");
          }
        }while (resultSet.next());
      }
    } catch (SQLException e) {
      System.err.println("Error validando la informacion" + e);
    }
    mysql.cerrarMysql();
  }
}
