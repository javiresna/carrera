package co.com.clases;

import co.com.dto.*;
import co.com.utils.Util;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class Juego {

  private ArrayList<Pista> pistas;
  private ArrayList<Conductor> conductores;
  private Jugador jugador;
  private Util util;
  private BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
  private int pistaSeleccionada;
  private Pista pistaCorrer;
  private Resultado resultado;

  public Juego() {
    pistas = new ArrayList<>();
    conductores = new ArrayList<>();
    util = new Util();
    jugador = new Jugador();
    resultado = new Resultado();
  }

  public void crearPistas() {
    for (int i = 0; i < 6; i++) {
      Pista pista = new Pista();
      pista.setCantidadCarriles(util.calcularCarriles());
      pista.setDistancia(util.calcularDistanciaKm());
      pista.setNombre("Pista # " + (i + 1));
      pistas.add(pista);
    }
  }

  public void crearConductores() {
    for (int i = 0; i < 10; i++) {
      Conductor conductor = new Conductor();
      conductor.setNombre(util.conductores().get(i));
      conductor.setEsJugador(false);
      conductores.add(conductor);
    }
  }

  public void iniciarJuego() throws IOException {
    System.out.println("Inicia el juego de Carreras");
    System.out.println("Ingresa tu nombre jugador");
    String nombre = teclado.readLine();
    jugador.setNombre(nombre);
    jugador.setCarril(-1);
    System.out.println("Hola " + nombre + " selecciona la pista en la que deseas correr");
    imprimirPistas();
  }

  public void imprimirPistas() {
    for (int i = 0; i < pistas.size(); i++) {
      System.out.println(pistas.get(i).getNombre() + " Carriles: " + pistas.get(i)
          .getCantidadCarriles() + " Distancia: " + pistas.get(i).getDistancia() + "Km");
    }
    seleccionarPista();
  }

  public void seleccionarPista() {
    System.out.println("Ingresa el numero de la pista en la que quieres correr");
    try {
      pistaSeleccionada = (Integer.parseInt(teclado.readLine()) - 1);
      if (pistaSeleccionada >= 0 && pistaSeleccionada <= 5) {
        pistaCorrer = pistas.get(pistaSeleccionada);
        System.out.println("Seleccionaste la pista # " + (pistaSeleccionada + 1));
        jugadorComoCorredor();
      } else {
        System.out.println("La pista seleccionada no existe intenta de nuevo");
        seleccionarPista();
      }
    } catch (Exception e) {
      System.err.println("Ingresaste un numero de pista erroneo intena de nuevo");
      seleccionarPista();
    }
  }

  public void jugadorComoCorredor() throws IOException {
    System.out.println("Deseas jugar como Corredor s/n");
    String respuesta = teclado.readLine();
    if ("S".equalsIgnoreCase(respuesta)) {
      System.out.println("Seleecciona el carril donde quieres correr");
      System.out.println("Recuerda que la pista #" + (pistaSeleccionada + 1) + " tiene " +
          pistaCorrer.getCantidadCarriles() + " Carriles");
      seleccionarCarril();
    } else {
      System.out.println("Se seleccionaran los conductores de manera aleatoria");
      llenarCarriles();
    }
  }

  public void seleccionarCarril() {
    System.out.println("Ingresa el carril donde quieres correr");
    try {
      int carril = (Integer.parseInt(teclado.readLine()) - 1);
      if (carril >= 0 && carril <= (pistaCorrer.getCantidadCarriles() - 1)) {
        jugador.setCarril(carril);
        System.out.println("Seleccionaste el carril numero " + (carril + 1));
        llenarCarriles();
      } else {
        System.err.println("El carril ingresado no existe");
        jugador.setCarril(-1);
        seleccionarCarril();
      }
    } catch (Exception e) {
      System.err.println("Ingresaste un carril erroneo intenta de nuevo");
      seleccionarCarril();
    }
  }

  public void llenarCarriles() {
    for (int i = 0; i < pistaCorrer.getCantidadCarriles(); i++) {
      if (jugador.getCarril() != -1 && jugador.getCarril() == i) {
        Conductor conductor = volverJugadorConductor();
        Carro carro = llenarCarro(conductor, i);
        Carril carril = llenarCarril(carro, i);
        pistaCorrer.getCarriles().add(carril);
      } else {
        Carro carro = llenarCarro(conductores.get(i), i);
        Carril carril = llenarCarril(carro, i);
        pistaCorrer.getCarriles().add(carril);
      }
    }
    mostrarCarriles();
  }

  public void mostrarCarriles() {
    System.out.println("Los carriles se llenaron de la siguiente manera");
    for (int i = 0; i < pistaCorrer.getCarriles().size(); i++) {
      System.out.println("En el carril numero " + pistaCorrer.getCarriles().get(i).getNumeroCarril()
          + " va el conductor: " + pistaCorrer.getCarriles().get(i).getCarro().getConductor()
          .getNombre()
          + " con carro numero " + pistaCorrer.getCarriles().get(i).getCarro().getNumeroCarro());
    }
    iniciarCarrera();
  }

  public void iniciarCarrera() {
    System.out.println("La carrera inicia ...........");
    boolean temino = true;
    while (temino) {
      for (int i = 0; i < pistaCorrer.getCarriles().size(); i++) {
        pistaCorrer.calularDistanciaCarroMetros(pistaCorrer.getCarriles().get(i));
      }
      temino = validarFinCarrera();
    }
    System.out.println("La carrera Termino ............");
    mostrarGanador();
  }

  public boolean validarFinCarrera() {
    int distanciaPistaMetros = pistaCorrer.getDistancia() * 1000;
    for (int i = 0; i < pistaCorrer.getCarriles().size(); i++) {
      Carro carro = pistaCorrer.getCarriles().get(i).getCarro();
      if (distanciaPistaMetros < carro.getDistanciaRecorrida() && !validarPodio(
          pistaCorrer.getCarriles().get(i))) {
        return llenarPodio(pistaCorrer.getCarriles().get(i));
      }
    }
    return true;
  }

  public boolean validarPodio(Carril carril) {
    if (validarHayCorredorPodio(pistaCorrer.getPodio().getPrimerLugar())) {
      if (carril.getCarro().getNumeroCarro() == pistaCorrer.getPodio().getPrimerLugar()
          .getNumeroCarro()) {
        return true;
      }
    }
    if (validarHayCorredorPodio(pistaCorrer.getPodio().getSegundoLugar())) {
      if (carril.getCarro().getNumeroCarro() == pistaCorrer.getPodio().getSegundoLugar()
          .getNumeroCarro()) {
        return true;
      }
    }
    if (validarHayCorredorPodio(pistaCorrer.getPodio().getTercerLugar())) {
      if (carril.getCarro().getNumeroCarro() == pistaCorrer.getPodio().getTercerLugar()
          .getNumeroCarro()) {
        return true;
      }
    }

    return false;
  }

  public boolean validarHayCorredorPodio(Carro carro) {
    if (carro != null) {
      return true;
    } else {
      return false;
    }
  }


  public boolean llenarPodio(Carril carril) {

    if (pistaCorrer.getPodio().getPrimerLugar() == null) {
      pistaCorrer.getPodio().setPrimerLugar(carril.getCarro());
    } else if (pistaCorrer.getPodio().getSegundoLugar() == null) {
      pistaCorrer.getPodio().setSegundoLugar(carril.getCarro());
    } else if (pistaCorrer.getPodio().getTercerLugar() == null) {
      pistaCorrer.getPodio().setTercerLugar(carril.getCarro());
    } else {
      return false;
    }
    return true;
  }

  public void mostrarGanador() {
    System.out.println("La carrera termino asi: ");
    System.out.println(
        "Primer Lugar: " + pistaCorrer.getPodio().getPrimerLugar().getConductor().getNombre()
            + " en el carro numero: " + pistaCorrer.getPodio().getPrimerLugar().getNumeroCarro());
    System.out.println(
        "Segundo Lugar: " + pistaCorrer.getPodio().getSegundoLugar().getConductor().getNombre()
            + " en el carro numero: " + pistaCorrer.getPodio().getSegundoLugar().getNumeroCarro());
    System.out.println(
        "Tercer Lugar: " + pistaCorrer.getPodio().getTercerLugar().getConductor().getNombre()
            + " en el carro numero: " + pistaCorrer.getPodio().getTercerLugar().getNumeroCarro());
    resultado.registrarResultadosCarrera(pistaCorrer.getPodio());
    resultado.registrarPrimerLugar(pistaCorrer.getPodio().getPrimerLugar());
  }

  public Conductor volverJugadorConductor() {
    Conductor conductor = new Conductor();
    conductor.setNombre(jugador.getNombre());
    conductor.setEsJugador(true);
    return conductor;
  }


  public Carro llenarCarro(Conductor conductor, int numeroCarro) {
    Carro carro = new Carro();
    carro.setConductor(conductor);
    carro.setDistanciaRecorrida(0);
    carro.setNumeroCarro(numeroCarro);
    return carro;
  }

  public Carril llenarCarril(Carro carro, int numeroCarril) {
    Carril carril = new Carril();
    carril.setCarro(carro);
    carril.setNumeroCarril(numeroCarril + 1);
    return carril;
  }


}
