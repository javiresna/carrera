package co.com.db;

import java.sql.*;

public class ConexionMysql {
    private static Connection conn;
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String USER = "root";
    private static final String PASS = "";
    private static final String URL = "jdbc:mysql://localhost:3306/carreras";

    public void conectarMysql() {
        conn = null;
        try {
            Class.forName(DRIVER);
            conn = DriverManager.getConnection(URL, USER, PASS);
            if (conn != null) {
                System.out.println("Conexion a mysql establecida .....");
            }
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("error al conectar con mysql :" + e);
        }
    }

    public  void cerrarMysql() {
        conn = null;
        System.out.println("Conexion a mysql cerrada .....");
    }

    public Connection getConnection() {
        return conn;
    }

    public boolean ejecutarConsulta(String sql) {
        try {
            Statement sentencia = getConnection()
                    .createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            sentencia.executeUpdate(sql);
            sentencia.close();
        } catch (SQLException e) {
            System.err.println("Error ejecutando la consulta: " + e);
            return false;
        }
        return true;
    }

    public ResultSet consultar(String sql) {
        ResultSet resultado;
        try {
            Statement sentencia = getConnection()
                    .createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            resultado = sentencia.executeQuery(sql);

        } catch (SQLException e) {
            System.err.println("Error ejecutando la consulta: " + e);
            return null;
        }
        return resultado;
    }

}
