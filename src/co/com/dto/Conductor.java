package co.com.dto;

public class Conductor {

  private String nombre;
  private boolean esJugador;

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public boolean isEsJugador() {
    return esJugador;
  }

  public void setEsJugador(boolean esJugador) {
    this.esJugador = esJugador;
  }
}
