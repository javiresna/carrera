package co.com.dto;

public class Jugador {
    private String nombre;
    private int carril;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCarril() {
        return carril;
    }

    public void setCarril(int carril) {
        this.carril = carril;
    }
}
