package co.com.dto;

public class Podio {
  private Carro primerLugar;
  private Carro segundoLugar;
  private Carro tercerLugar;


  public Carro getPrimerLugar() {
    return primerLugar;
  }

  public void setPrimerLugar(Carro primerLugar) {
    this.primerLugar = primerLugar;
  }

  public Carro getSegundoLugar() {
    return segundoLugar;
  }

  public void setSegundoLugar(Carro segundoLugar) {
    this.segundoLugar = segundoLugar;
  }

  public Carro getTercerLugar() {
    return tercerLugar;
  }

  public void setTercerLugar(Carro tercerLugar) {
    this.tercerLugar = tercerLugar;
  }
}
