package co.com.dto;

import co.com.utils.Util;
import java.util.ArrayList;

public class Pista {
    // nombre de la pista
    private String nombre;
    // distancia de la pista en kilometros
    private int distancia;
    // cantidad de carriles por pista
    private int cantidadCarriles;

    private ArrayList<Carril> carriles;
    private Podio podio;
    private Util util;

    public Pista() {
        podio = new Podio();
        util = new Util();
        carriles = new ArrayList<>();
    }

    public void calularDistanciaCarroMetros(Carril carril) {
        int distanciaActual = carril.getCarro().getDistanciaRecorrida();
        int nuevaDistancia = util.dado6Caras() * 100;
        carril.getCarro().setDistanciaRecorrida(distanciaActual + nuevaDistancia);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getDistancia() {
        return distancia;
    }

    public void setDistancia(int distancia) {
        this.distancia = distancia;
    }

    public ArrayList<Carril> getCarriles() {
        return carriles;
    }

    public int getCantidadCarriles() {
        return cantidadCarriles;
    }

    public void setCantidadCarriles(int cantidadCarriles) {
        this.cantidadCarriles = cantidadCarriles;
    }

    public Podio getPodio() {
        return podio;
    }
}
