import co.com.clases.Juego;


public class Main {

    public static void main(String[] args) {
        Juego juego = new Juego();
        juego.crearPistas();
        juego.crearConductores();
        try {
            juego.iniciarJuego();
        } catch (Exception e) {
            System.err.println("Error en ejecucion del juego");
        }

    }
}
